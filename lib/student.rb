class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    self.courses.each { |course| conflict? if new_course.conflicts_with?(course)}
    self.courses << new_course unless self.courses.include?(new_course)
    new_course.students << self
  end

  def conflict?
    raise 'Course has time conflict with other course!!'
  end

  def course_load
    department_hash = Hash.new(0)
    @courses.each do |course|
      department_hash[course.department] += course.credits
    end
    department_hash
  end
end
